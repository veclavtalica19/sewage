extends Node

# Needs to be kept in check with Nim definition
# todo: Generate this from Nim?
enum {
  ckSpeck,
  ckBlock,
  ckThing,
  ckEdge,
}
