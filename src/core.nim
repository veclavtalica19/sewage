when not defined(release):
  import segfaults # converts segfaults into NilAccessError

# todo: godot-nim tries to define gdobj again if its imported multiple times in unrelated modules, which is quite troublesome
# import nodes/worldserver
import nodes/worldcanvas
