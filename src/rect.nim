import ./vec

type
  IRect* = tuple
    ## Defines rectangle from segment
    pos: IVec
    dims: IVec

{.push inline.}

converter toIVec*(a: IRect): IVec =
  (a.pos.x + a.dims.x, a.pos.y + a.dims.y)

func fit*(a: IRect, dims: IVec): IRect =
  result = a
  assert a.dims < dims
  if a.pos.x < 0:
    result.pos.x = 0
  elif a.pos.x + a.dims.x >= dims.x:
    result.pos.x -= a.pos.x + a.dims.x - dims.x
  if a.pos.y < 0:
    result.pos.y = 0
  elif a.pos.y + a.dims.y >= dims.y:
    result.pos.y -= a.pos.y + a.dims.y - dims.y

func contains*(a: IVec, item: IRect): bool =
  item.toIVec < a

{.pop.}

# todo: godot type info
