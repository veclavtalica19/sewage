import pkg/godot

# todo: Suboptimal. We might consider just using float32 based color
#       There's also an alternative to use pallet based color scheme, where each color represented by index
#       This strategy is superior in most ways, except customizability
#       But we also could reserve dynamic range to redefine on runtime, similar to how block registry is done

type
  ColorIdx* = distinct uint16
  Color = array[4, byte]
  ColorName* {.size: sizeof(uint16).} = enum 
    cnOpaque
    cnBlack
    cnDarkGrey
    cnGrey
    cnGreen

func color(r, g, b: byte): Color = [r, g, b, 255]

const color_registry = [
  cnOpaque: Color.default,
  cnBlack: color(0, 0, 0),
  cnDarkGrey: color(127, 127, 127),
  cnGrey: color(63, 63, 63),
  cnGreen: color(0, 255, 0),
]

const color_registry_godot = block: static:
  var registry: array[color_registry.len, godot.Color]
  for i, c in color_registry:
    registry[i.uint16] = initColor(
      c[0].float32 / 255.float32,
      c[1].float32 / 255.float32,
      c[2].float32 / 255.float32,
      c[3].float32 / 255.float32)
  registry

{.push inline.}

converter toGodot*(a: ColorIdx): godot.Color =
  # todo: Bypass bound checking? We could do it if we allocate arrays big enough to cover the whole range of indexes
  color_registry_godot[a.uint16]

converter toColorIdx*(a: ColorName): ColorIdx = a.ColorIdx

{.pop.}
