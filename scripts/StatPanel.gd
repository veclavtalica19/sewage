extends Container

func _process(_d: float) -> void:
  $VBoxContainer/FPS.text = "FPS: " + str(Engine.get_frames_per_second())
