import color
export color

type
  Block* = distinct uint64

  BlockDescriptor = object
    name: string
    color: ColorIdx
    desc: string

const registry = [
  BlockDescriptor(
    name: "unbreakium",
    color: cnGrey,
    desc: "the only thing keeping us from the world"
  ),
  BlockDescriptor(
    name: "bullshit",
    color: cnDarkGrey,
    desc: "useless piece of matter"
  ),
]

# todo: Use pure enums
const
  bkUnbreakium* = 0.Block
  bkBullshit*   = 1.Block

# todo: For modding and dynamically generating content
var dyn_registry: seq[BlockDescriptor]

# todo: Should be able to access separate runtime editable vector of descriptors
# todo: For static Block access we can precompute the address
template implGetter(name: untyped): untyped =
  proc `name`*(a: static[Block]): BlockDescriptor.`name` {.inject, inline.} =
    static: doAssert a.uint64 < registry.len.uint64
    registry[a.uint64].`name`

  proc `name`*(a: Block): BlockDescriptor.`name` {.inject, inline.} =
    assert a.uint64 < registry.len.uint64
    registry[a.uint64].`name`

implGetter name
implGetter color
implGetter desc
