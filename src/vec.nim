import std/math
import pkg/godot

# todo: Negative values are not handled properly
#       We might demand that only positive range of int32 should be used, for performance reasons
#       Its also just that we dont really need to have negative vectors, as we're assuming that top-left corner is origin in most places

type
  IVec* = tuple
    ## Two dimensional vector with assumed origin of (0, 0)
    x: int32
    y: int32

{.push inline.}

func width*(v: IVec): int32 = v.x
func height*(v: IVec): int32 = v.y
func `width=`*(v: var IVec, val: int32) = v.x = val
func `height=`*(v: var IVec, val: int32) = v.y = val

# todo: Define via macro
func `+`*(a, b: IVec): IVec = (a.x + b.x, a.y + b.y)
func `+`*(a: IVec, s: int32): IVec = (a.x + s, a.y + s)
func `div`*(a, b: IVec): IVec = (a.x div b.x, a.y div b.y)
func `div`*(a: IVec, s: int32): IVec = (a.x div s, a.y div s)
func `mod`*(a: IVec, s: int32): IVec = (a.x mod s, a.y mod s)

func `==`*(a: IVec, s: int32): bool = a.x == s and a.y == s
func `>=`*(a: IVec, s: int32): bool = a.x >= s and a.y >= s

func area*(v: IVec): int32 = abs v.x * abs v.y
func contains*(a, item: IVec): bool =
  if a >= 0 and item >= 0:
    item.x < a.x and item.y < a.y
  else:
    # todo: We might consider implementing areas on negative ranges, but its a bit trickier
    false

func clamp*(item, b: IVec): IVec =
  # if item.x < 0:
  #   result.x = 0
  # if item.y < 0:
  #   result.y = 0
  if item.x >= b.x:
    result.x = b.x - 1
  if item.y >= b.y:
    result.y = b.y - 1

func inRectOf*(item, a: IVec): bool =
  if item.x == 0 or item.x == a.x - 1:
    if item.y == 0: true
    elif item.y < a.y: true
    else: false
  elif item.y == 0 or item.y == a.y - 1:
    if item.x == 0: true
    elif item.x < a.x: true
    else: false
  else: false

template transform*(a: IVec, t: untyped): untyped = (a.x.t, a.y.t)

iterator items*(a: IVec): IVec =
  # todo: Will break on negative values
  for x in 0..<a.width:
    for y in 0..<a.height:
      yield (x, y)

iterator offset*(a, offset: IVec): IVec =
  assert offset in a
  for x in offset.x..<a.width:
    for y in offset.y..<a.height:
      yield (x, y)

iterator rect*(a: IVec): IVec =
  for x in 0..<a.width:
    yield (x, 0i32)
  for x in 0..<a.width:
    yield (x, a.y - 1)
  for y in 0..<a.height:
    yield (0i32, y)
  for y in 0..<a.height:
    yield (a.x - 1, y)

converter toGodot*(a: IVec): Vector2 =
  vec2(float32 a.x, float32 a.y)

converter fromGodot*(a: Vector2): IVec =
  (a.x.int32, a.y.int32)

{.pop.}

func godotTypeInfo*(T: typedesc[IVec]): GodotTypeInfo =
  result.variantType = VariantType.Vector2

proc toVariant*(self: IVec): Variant =
  newVariant(vec2(float32 self.x, float32 self.y))

proc fromVariant*(self: var IVec, v: Variant): ConversionResult =
  let vec = v.asVector2
  when not defined(release):
    # As we only have access to floating point vector - check whether values are integer
    if vec.x.ceil != vec.x or vec.y.ceil != vec.y:
      return ConversionResult.RangeError
  self = vec
